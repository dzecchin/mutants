/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package datamodel;

/**
 * Bean representing a DNA Sequence
 * @author dzecchin
 */
public class DnaSequence {
    
    String[] dna;

    public String[] getDna() {
        return dna;
    }

    public void setDna(String[] dna) {
        this.dna = dna;
    }
    
    public int getStringLength()
    {
        return(dna.length>0?dna[0].length():0);
    }
    
    public String getStringDna()
    {
        StringBuffer buffer = new StringBuffer();
        
        for (int i=0; i < dna.length; i++)
        {
            buffer.append(dna[i]);
        }
        return(buffer.toString());
    }
    
}
