/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package datamodel;

import java.util.concurrent.atomic.AtomicLong;

/**
 * Singleton to cache DNA tests stats
 * @author dzecchin
 */
public class MutantStats {
    
    /**
     * Singleton instance
     */
    private static MutantStats instance = new MutantStats();
    private AtomicLong humanDnaSequences, mutantDnaSequences;
    
    /**
     * Singleton private constructor
     */
    private MutantStats(){
        this.humanDnaSequences = new AtomicLong(0);
        this.mutantDnaSequences = new AtomicLong(0);
    };
    
    /**
     * Static 'instance' method
     * @return a MutantStats instance
     */
    public static MutantStats getInstance()
    {
        return(instance);
    }

    public long getHumanDnaSequences() {
        return (humanDnaSequences.get());
    }

    public long getMutantDnaSequences() {
        return (mutantDnaSequences.get());
    }
    
    public void setMutantDnaSequences(long value) {
        this.mutantDnaSequences.set(value);
    }

    public void setHumanDnaSequences(long value) {
        this.humanDnaSequences.set(value);
    }
    
    public long incHumanDnaSequence(){
        return (this.humanDnaSequences.incrementAndGet());
    }
    
    public long incMutantDnaSequence(){
        return (this.mutantDnaSequences.incrementAndGet());
    }
    
    public float getRatio(){
        float mutants = this.mutantDnaSequences.floatValue();
        float humans = this.humanDnaSequences.floatValue();
                
        return( humans > 0 ? mutants/humans : 0 );
    }
}
