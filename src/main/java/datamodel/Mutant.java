/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package datamodel;

import com.abahgat.suffixtree.GeneralizedSuffixTree;
import iterator.ColumnIterator;
import iterator.DiagonalIterator;
import iterator.ReverseDiagonalIterator;
import iterator.RowIterator;
import iterator.StringMatrixIterator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 *
 * @author dzecchin
 */
public class Mutant {
    
    private final Logger log = LoggerFactory.getLogger(this.getClass());
    
    public static final String[] dnaMutantSequences = {"AAAA", "CCCC", "GGGG", "TTTT"};
    public static int mutantDnaSequenceLenght = 4;
    
    /**
     * Singleton instance
     */
    private static Mutant singleton = new Mutant();

   /**
    * Singleton private constructor
    */
   private Mutant(){};

   /**
    * Static 'instance' method 
     * @return 
    */
   public static Mutant getInstance( ) {
      return singleton;
   }
    
    private void populateTree(GeneralizedSuffixTree tree, StringMatrixIterator iterator)
    {
        int index = tree.getLastIndex() + 1;
        while ( !iterator.hasFinished() )
        {
            tree.put(iterator.getElement(),index);
            iterator.next();
            index++;
        }
    }
    
    public boolean isMutant(String[] dna)
    {
        int mutantSequenceCount = 0, 
            index = 0;
        GeneralizedSuffixTree suffixTree = new GeneralizedSuffixTree();
        
        try{
//            long time1 = System.nanoTime();
            
            /* Populate suffix tree with all matrix strings */
            populateTree(suffixTree, new RowIterator(dna));
            populateTree(suffixTree, new ColumnIterator(dna));
            populateTree(suffixTree, new DiagonalIterator(dna, mutantDnaSequenceLenght));
            populateTree(suffixTree, new ReverseDiagonalIterator(dna, mutantDnaSequenceLenght));


            /* While we do not found enough mutants sequences and we do not checks every mutant dna sequence */
            while ( mutantSequenceCount <= 1 && index < dnaMutantSequences.length )
            {
                /* Search a mutant dna sequence in the suffix tree and get its count */
                mutantSequenceCount += suffixTree.search(dnaMutantSequences[index]).size();
                index++;
            }
            
            
//            long time2 = System.nanoTime();
//            log.debug("Tiempo: " + ((time2 - time1)/1000));
        }
        /* If matrix is not squere, we catch StringIndexOutOfBoundsException or ArrayIndexOutOfBoundsException */
        catch (StringIndexOutOfBoundsException | ArrayIndexOutOfBoundsException e){
            log.error("Not square DNA matrix received");
            mutantSequenceCount = 0;
        }
        
        return(mutantSequenceCount > 1);
    }

    /**
     * Test main
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        String[] dna = {"ATGTGA","CATTGC","TTATGT","TGAAGG","CCCCTA","TCACTG"};
        
        Mutant aMutan = new Mutant();
        
        System.out.println(aMutan.isMutant(dna));
       
    }
    
}
