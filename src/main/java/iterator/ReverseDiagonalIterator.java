/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package iterator;

/**
 *
 * @author dzecchin
 */
public class ReverseDiagonalIterator extends DiagonalIterator{

    /**
     * Creates a ReverseDiagonalIterator
     * @param matrix
     * @param minLength  the minimun length accepted for a string element of the matrix
     */
    public ReverseDiagonalIterator(String[] matrix, int minLength)
    {
        super(matrix, minLength);
    }
    
    @Override
    public void first()
    {
        /* We set actualRow to the shortest rigth-left diagonal string index to start */
        this.actualRow = this.matrix.length - this.minLength;
        /* We set actualColumn to the greatest column index */
        this.actualColumn = this.matrix.length - 1;
        this.elementsRetrieved = 0;
    }
    
    @Override
    public void incIndex() {
        
        /* If we have rows to transverse */
        if ( this.actualRow > 0 )
        {
            this.actualRow--;
        }
        /* Else if we have columns to transverse */
        /* We decrement actualColumn up to the column index that represents the shortest rigth-left diagonal string index minux 1 (arrays starts from 0)*/
        else if (this.actualColumn > (this.minLength - 1) )
        {
            this.actualColumn--;
        }
        
    }
    
    /**
     * Template method member that evaluates if we have finished to retrieve a diagonal string
     * @param row
     * @param column
     * @return 
     */
    @Override
    protected boolean diagonalLoopFinished(int row, int column)
    {
        /* If row is lower than matrix length and column is greater or equal than 0, return true */
        return(row < this.matrix.length && column >= 0);
    }
    
    /**
     * Template method member to update instance index for string diagonally retrieval
     */
    @Override
    protected void updateLoopIndexes()
    {
        /* Transverse diagonall from right to left: increment row, decrement column */
        this.loopRowIndex++;
        this.loopColumnIndex--;
    }
}
