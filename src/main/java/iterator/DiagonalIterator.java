/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package iterator;

/**
 *
 * @author dzecchin
 */
public class DiagonalIterator extends StringMatrixIterator{


    protected int totalDiagonals, minLength, loopRowIndex, loopColumnIndex;
    
    /**
     * Crates a DiagonalIterator
     * @param matrix
     * @param minLength the minimun length accepted for a string element of the matrix
     */
    public DiagonalIterator(String[] matrix, int minLength)
    {
        this.matrix = matrix;
        this.minLength = minLength;
        this.first();
        /* The total number of diagonal strings depends of the string retrieved length in "minLength" arg*/
        this.totalDiagonals = ((this.matrix.length - minLength) * 2 ) + 1;
    }
    

    @Override
    public void first()
    {
        /* We set actualRow to the shortest left-rigth diagonal string index to start */
        this.actualRow = this.matrix.length - this.minLength;
        this.actualColumn = 0;
        this.elementsRetrieved = 0;
    }
    
    @Override
    public void incIndex() {
        
        /* If we have rows remaining, decrement actualRow */
        if ( this.actualRow > 0 )
        {
            this.actualRow--;
        }
        /* Else if we have columns remaining, decrement actualColumn */
        /* We increment actualColumn up to the column index that represents the shortest left-righ diagonal string index */
        else if (this.actualColumn < this.matrix.length - this.minLength )
        {
            this.actualColumn++;
        }
        
    }
    
    /**
     * Template method to retrieve actual string element
     * @return String actual diagonal string element from matrix
     */
    @Override
    public String getElement() {
        
        StringBuffer buffer = new StringBuffer();
        
        /* Set private instance index to transverse diagonally */
        this.loopRowIndex = this.actualRow;
        this.loopColumnIndex = this.actualColumn;
        
        for (; this.diagonalLoopFinished(this.loopRowIndex, this.loopColumnIndex); this.updateLoopIndexes())
        {
            buffer.append(this.matrix[this.loopRowIndex].charAt(this.loopColumnIndex));
        }
        
        return(buffer.toString());
    }
    
    protected boolean diagonalLoopFinished(int row, int column)
    {
        /* If row and column are lower than matrix length return true */
        return(row < this.matrix.length && column < this.matrix.length);
    }
    
    protected void updateLoopIndexes()
    {
        /* Transverse diagonally from left to right: increment row, increment column */
        this.loopRowIndex++;
        this.loopColumnIndex++;
    }
    
    /**
     * Template method member to update instance index for string diagonally retrieval
     */
    @Override
    public boolean hasFinished()
    {
        return(this.elementsRetrieved == this.totalDiagonals);
    }
    
}
