package iterator;

/**
 * Iterator pattenr for transverse a square string matrix
 * 
 * @author dzecchin
 */
public abstract class StringMatrixIterator {
    
    protected String[] matrix;
    protected int actualRow, actualColumn, elementsRetrieved;
    
    /**
     * Get actual string from matrix
     * @return
     * @throws ArrayIndexOutOfBoundsException
     * @throws StringIndexOutOfBoundsException 
     */
    public abstract String getElement() throws ArrayIndexOutOfBoundsException, StringIndexOutOfBoundsException;
    
    /**
     * Set the iterator to first element
     */
    public abstract void first();
    
    /**
     * Template method member to advance to next matrix item
     */
    protected abstract void incIndex();
    
    /**
     * Set the next string element from matrix to be retrieved
     */
    public void next()
    {
        this.incIndex();
        this.elementsRetrieved++;

    }
    
    /**
     * Checks if we iterate through all string elements from matrix
     * @return boolean true if we retrieved all strings from matrix
     */
    public boolean hasFinished()
    {
        return(this.elementsRetrieved == this.matrix.length );
    }
}
