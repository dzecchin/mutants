/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package iterator;

/**
 *
 * @author dzecchin
 */
public class ColumnIterator extends StringMatrixIterator{
   
    
    public ColumnIterator(String[] matrix)
    {
        this.matrix = matrix;
        this.first();
    }
    
    public void first()
    {
        this.actualRow = 0;
        this.actualColumn = 0;
        this.elementsRetrieved = 0;
    }
    
    public void incIndex()
    { 
        /* If we have columns remaining */
        if ( this.matrix.length - 1 > actualColumn )
        {
            this.actualColumn++;
        }
    }
    
    public String getElement()
    {
        StringBuffer buffer = new StringBuffer();
        
        /* Build a string with chars from column "actualColumn" */
        for (int row=0; row < this.matrix.length; row++)
        {
            buffer.append(this.matrix[row].charAt(actualColumn));
        }
        
        return(buffer.toString());
    }
    
        
}
