/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package iterator;

/**
 * String matrix row iterator
 * @author dzecchin
 */
public class RowIterator extends StringMatrixIterator{
    
    public RowIterator(String[] matrix)
    {
        this.matrix = matrix;
        this.first();
    }
    
    public void first()
    {
        this.actualRow = 0;
        this.actualColumn = 0;
        this.elementsRetrieved = 0;
    }
    
    public void incIndex()
    { 
        /* If we have rows remaining */
        if ( this.matrix.length - 1 > actualRow )
        {
            this.actualRow++;
        }
    }

    public String getElement()
    {
        /* Returns the string represented by matrix[actualRow] */
        return(this.matrix[actualRow]);
    }
}
