/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package restController;

import datamodel.MutantStats;
import javax.annotation.PostConstruct;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.ResponseBody;
import repo.DnaRepository;

@RestController
public class StatsController {

    @Autowired
    private DnaRepository dnaRepository;
    
    @PostConstruct
    private void init() {
        
        /* When init, we retrieve from DB DNA sequences count from human and mutant*/
        MutantStats.getInstance().setHumanDnaSequences(dnaRepository.getHumanDnaCount());
        MutantStats.getInstance().setMutantDnaSequences(dnaRepository.getMutantDnaCount());
    }

    @RequestMapping(value = "/stats", method = RequestMethod.GET)
    @ResponseBody
    public MutantStats getStats() {
        return MutantStats.getInstance();
    }
}

