/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package restController;

import repo.DnaRepository;
import datamodel.DnaSequence;
import datamodel.Mutant;
import datamodel.MutantStats;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.ResponseBody;

@RestController
public class MutantController {

    @Autowired
    private DnaRepository dnaRepository;
    
    @RequestMapping(value = "/mutant", method = RequestMethod.POST)
    @ResponseBody
    public ResponseEntity isMutant(@RequestBody DnaSequence dnaInput) {
        
        ResponseEntity response;
        Mutant aMutant = Mutant.getInstance();
        MutantStats stats = MutantStats.getInstance();
        Boolean absent;
        
        /* If we receive a valid string DNA matrix and is a mutant DNA */
        if ( dnaInput.getStringLength() >= Mutant.mutantDnaSequenceLenght && aMutant.isMutant(dnaInput.getDna()) )
        {
            /* Save mutant dna sequence */
            absent = dnaRepository.saveIfAbsent(dnaInput, true);
            
            /* If was absent, update cached stats */
            if ( absent )
            {
                stats.incMutantDnaSequence();
            }
            
            /* Returns HTTP 200-OK */
            response = new ResponseEntity(HttpStatus.OK);
        }
        else 
        {
            /* Save human dna sequence */
            absent = dnaRepository.saveIfAbsent(dnaInput, false);
            
            /* If was absent, update cached stats */
            if ( absent )
            {
                stats.incHumanDnaSequence();
            }
            
            /* Returns HTTP 403-FORBIDDEN */
            response = new ResponseEntity(HttpStatus.FORBIDDEN);
        }
        
        return response;
    }
}

