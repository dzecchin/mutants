/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package repo;

import datamodel.DnaSequence;

/**
 *
 * @author dzecchin
 */
public interface DnaRepository {
    
    public Boolean saveIfAbsent(DnaSequence aDna, boolean isMutant);
    public long getMutantDnaCount();
    public long getHumanDnaCount();
}
