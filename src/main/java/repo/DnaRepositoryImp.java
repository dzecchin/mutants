/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package repo;

import datamodel.DnaSequence;
import javax.annotation.PostConstruct;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.redis.core.HashOperations;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Repository;

/**
 *
 * @author dzecchin
 */
@ComponentScan("config")
@Repository
public class DnaRepositoryImp implements DnaRepository{

    private static final String mutantKey = "mutantDna";
    private static final String humanKey = "humanDna";
    
    private RedisTemplate<String, Object> redisTemplate;
    private HashOperations<String, String, Boolean> hashOperations;
    
    @Autowired
    public DnaRepositoryImp(RedisTemplate<String, Object> redisTemplate) {
            this.redisTemplate = redisTemplate;
    }
    
    @PostConstruct
    private void init() {
            hashOperations = redisTemplate.opsForHash();
    }
    
    @Override
    public Boolean saveIfAbsent(DnaSequence aDna, boolean isMutant) {
        redisTemplate.opsForValue().setIfAbsent(aDna.getStringDna(), isMutant);
        Boolean result;
        if ( isMutant )
            result = hashOperations.putIfAbsent(mutantKey, aDna.getStringDna(), isMutant);
        else
            result = hashOperations.putIfAbsent(humanKey, aDna.getStringDna(), isMutant);
        
        return(result);
    }

    @Override
    public long getMutantDnaCount() {
        return(hashOperations.keys(mutantKey).size());
        
    }

    @Override
    public long getHumanDnaCount() {
       return(hashOperations.keys(humanKey).size());
    }

    
    
}
