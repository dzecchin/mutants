/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package exception;

/**
 *
 * @author dzecchin
 */

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

 
@ControllerAdvice
public class MutantServiceExceptionHandler extends ResponseEntityExceptionHandler
{
    private final Logger log = LoggerFactory.getLogger(this.getClass());
    
    @ExceptionHandler(Exception.class)
    public final ResponseEntity<Object> handleAllExceptions(Exception ex, WebRequest request) {
        log.error("Invalid request. HTTP Return code: " + HttpStatus.BAD_REQUEST);
        return new ResponseEntity(HttpStatus.BAD_REQUEST);
    }
 
    @Override
    protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {
        log.error("Invalid arguments. HTTP Return code: " + HttpStatus.BAD_REQUEST);
        return new ResponseEntity(HttpStatus.BAD_REQUEST);
    }
}