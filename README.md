# Mutants REST service

## Implementación

Para verificar la existencia de las secuencias ADN que definen a un mutante, se utilizó un árbol de sufijos general (https://en.wikipedia.org/wiki/Generalized_suffix_tree). El algoritmo consiste en:

1. Se recorre la matriz recibida para obtener las secuencias a verificar: filas, columnas y diagonales (izq-der y der-izq).
2. Se agregan estas secuencias al árbol. 
3. Se verifican las secuencias "AAAA", "CCCC", "GGGG" y "TTTT" en el árbol, si se encuentran dos, entonces el ADN es mutante.

El ábol tiene complejidad espacial y computacional de theta(n) y puede usarse para encontrar las N ocurrencias de una string en tiempo O(m+N), donde m es la longitud del string buscado (asumiendo alfabeto de tamaño constante)

## Funcionamiento

### ADN mutante:

Para comprobar si una secuencia ADN es de un mutante, se debe realizar un post al endpoint http://ec2-177-71-204-191.sa-east-1.compute.amazonaws.com:8080/mutant . 

Por ejemplo:
```
POST → /mutant/
{
	"dna":["ATGCGAA","ATGCGAA","CAGTGCA","ATTTTGT","CAGAAGG","TCCCCTA","GTCACTG"]
}
```
Es una secuencia ADN mutante.

En caso de verificar un mutante, la API responderá 200-OK. En caso contrario, si es humano, responderá 403-FORBIDDEN.

Nota: si se recibe una secuencia ADN incompleta (no es una matriz cuadrada), también se responderá 403-FORBIDDEN.

### ADN Stats:

Para consultar las estadísticas de las verificaciones de ADN, se debe realizar GET al endpoint http://ec2-177-71-204-191.sa-east-1.compute.amazonaws.com:8080/stats

La respuesta JSON es como sigue:
```
{
    "humanDnaSequences": 2,
    "mutantDnaSequences": 1,
    "ratio": 0.5
}
```
Indicando la cantidad de ADNs mutantes, humanos y la proporción entre ambos.

## Tecnologías utilizadas

- Java 1.8
- SpringBoot
- Redis
- Gradle
- AWS EC2
- Postman (Chrome extension)

## App Standalone

En "build/libs" se versiona un jar que puede ser ejecutado para correr la aplicación sin necesidad de un contenedor web java:

```
java -jar mutants-rest-service-0.1.0.jar
```

El único requisito es disponer el servicio Redis en ejecución en el puerto default.

## Mejoras

Implementar casos de tests automáticos con JUnit . Acoplar herramienta de integración continua.